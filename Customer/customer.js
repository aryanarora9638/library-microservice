//Imports
let express    = require('express')
let mongoose   = require('mongoose')
let bodyParser = require('body-parser')
let Customer       = require('./model/Customer')

//Initialize Express
let app = express()
app.use(bodyParser.json())

//Defining Server Port
app.listen(3002, () => {
    console.log("Server Running - Customer")
})

//Setup DB
mongoose.connect(
    'mongodb+srv://bookCluster:bookCluster@books-q9pd2.mongodb.net/test?retryWrites=true&w=majority',
    { useNewUrlParser: true },
    () => {
        console.log("Connect to DB - Customer")
    })

//Routes

//Main Home Route
app.get('/', (req, res) => {
    res.send("Home Page - Customer")
})

//GET - all books
app.get('/customer', async (req, res) => {
    let allCustomer = await Customer.find()
    res.send(allCustomer)
})

//GET - book by id
app.get('/customer/:id', async (req, res) => {
    let requiredCustomer = await Customer.findOne({_id : req.params.id})
    res.send(requiredCustomer)
})

//GET - '*' - unknown routes
app.get('*', (req, res) => {
    res.send("Unknown Page - Customer")
})

//POST - book
app.post('/new-customer', async (req, res) => {
    let newCustomer = {
        name : req.body.name,
        age : req.body.age,
        telephone: req.body.telephone
    }

    //Check if Customer (same combination) already exists
    let customerExist = await Customer.findOne(newCustomer)
    if(customerExist){
        res.status(400).send("Customer Already Exists")
    }
    else {
        //Customer doesn't exist
        newCustomer = new Customer(newCustomer)

        try {
            let savedCustomer = await newCustomer.save()
                .then(() => {
                    res.send("Customer Added")
                })
        }
        catch (e) {
            res.status(400).send(e)
            console.log("Invalid Input Fields - POST -> new-customer")
        }
    }

})

//DELETE - Customer by id
app.delete('/remove-customer/:id', async (req, res) => {
    let requiredCustomer = await Customer.findOneAndDelete({_id : req.params.id})
    if(!requiredCustomer){
        res.send("Requested Customer Wasn't Found")
        return
    }
    res.send("Customer Deleted")
})