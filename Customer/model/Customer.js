let mongoose = require('mongoose')

let customerSchema = new mongoose.Schema({
    name : {
        type: String,
        required : true
    },
    age : {
        type: Number,
        required: true
    },
    telephone : {
        type : Number,
        required : true
    }
})

module.exports = mongoose.model('Customer', customerSchema)