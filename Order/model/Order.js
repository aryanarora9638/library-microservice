let mongoose = require('mongoose')

let orderSchema = new mongoose.Schema({
    customerID : {
        type: mongoose.SchemaTypes.ObjectId,
        required: true
    },
    bookID : {
        type: mongoose.SchemaTypes.ObjectId,
        required: true
    },
    orderPlacedDate : {
        type: Date,
        required: true
    },
    deliveryDate : {
        type: Date,
        required: true
    }
})

module.exports = mongoose.model('Order', orderSchema)