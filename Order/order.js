//Imports
let axios      = require('axios')
let express    = require('express')
let mongoose   = require('mongoose')
let bodyParser = require('body-parser')
let Order      = require('./model/Order')

//Initialize Express
let app = express()
app.use(bodyParser.json())

//Defining Server Port
app.listen(3003, () => {
    console.log("Server Running - Order")
})

//Setup DB
mongoose.connect(
    'mongodb+srv://bookCluster:bookCluster@books-q9pd2.mongodb.net/test?retryWrites=true&w=majority',
    { useNewUrlParser: true },
    () => {
        console.log("Connect to DB - Order")
    })

//Routes

//Main Home Route
app.get('/', (req, res) => {
    res.send("Home Page - Order")
})

//GET - all books
app.get('/order', async (req, res) => {
    let allOrder = await Order.find()
    res.send(allOrder)
})

//GET - book by id
app.get('/order/:id', async (req, res) => {
    let requiredOrder = await Order.findOne({_id : req.params.id})
    res.send(requiredOrder)
})

//GET - '*' - unknown routes
app.get('*', (req, res) => {
    res.send("Unknown Page - Order")
})

//POST - book
app.post('/new-order', async (req, res) => {
    let newOrder = {
        customerID : mongoose.Types.ObjectId(req.body.customerID),
        bookID : mongoose.Types.ObjectId(req.body.bookID),
        orderPlacedDate: req.body.orderPlacedDate,
        deliveryDate: req.body.deliveryDate
    }

    //Check if the Customer exists
    try {
        let customerExist = await axios.get("http://localhost:3002/customer/" + newOrder.customerID)
        let bookExist = await axios.get("http://localhost:3001/book/" + newOrder.bookID)

        let orderExist
        if(customerExist.data && bookExist.data){
            orderExist = await Order.findOne(newOrder)
        }
        else {
            return res.send("Customer or/and Book Does not exist")
        }

        if(orderExist){
            res.send("Order Already Exists")
        }
        else {
            //Order doesn't exist
            newOrder = new Order(newOrder)
            try {
                let savedOrder = newOrder.save()
                    .then(() => {
                        res.send("Order Added")
                    })
            }
            catch (e) {
                res.send(e)
                console.log("Invalid Input Fields - POST -> new-Order")
            }
        }
    }
    catch (e) {
        res.send(e)
    }
})

//DELETE - Customer by id
app.delete('/remove-order/:id', async (req, res) => {
    let requiredOrder = await Order.findOneAndDelete({_id : req.params.id})
    if(!requiredOrder){
        res.send("Requested Order Wasn't Found")
        return
    }
    res.send("Order Deleted")
})