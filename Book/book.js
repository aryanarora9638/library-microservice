//Imports
let express    = require('express')
let mongoose   = require('mongoose')
let bodyParser = require('body-parser')
let Book       = require('./model/Book')

//Initialize Express
let app = express()
app.use(bodyParser.json())

//Defining Server Port
app.listen(3001, () => {
    console.log("Server Running - Book")
})

//Setup DB
mongoose.connect(
    'mongodb+srv://bookCluster:bookCluster@books-q9pd2.mongodb.net/test?retryWrites=true&w=majority',
    { useNewUrlParser: true },
    () => {
        console.log("Connect to DB - Book")
    })

//Routes

//Main Home Route
app.get('/', (req, res) => {
    res.send("Home Page - Book")
})

//GET - all books
app.get('/book', async (req, res) => {
    let allBooks = await Book.find()
    res.send(allBooks)
})

//GET - book by id
app.get('/book/:id', async (req, res) => {
    let requiredBook = await Book.findOne({_id : req.params.id})
    res.send(requiredBook)
})

//GET - '*' - unknown routes
app.get('*', (req, res) => {
    res.send("Unknown Page - Book")
})

//POST - book
app.post('/new-book', async (req, res) => {
    let newBook = {
        title : req.body.title,
        author : req.body.author,
        publication: req.body.publication
    }

    //Check if Book (same combination) already exists
    let bookExist = await Book.findOne(newBook)
    if(bookExist){
        res.status(400).send("Book Already Exists")
    }
    else {
        //book doesn't exist
        newBook = new Book(newBook)

        try {
            let savedBook = await newBook.save()
                .then(() => {
                    res.send("Book Added")
                })
        }
        catch (e) {
            res.status(400).send(e)
            console.log("Invalid Input Fields - POST -> new-book")
        }
    }

})

//DELETE - book by id
app.delete('/remove-book/:id', async (req, res) => {
    let requiredBook = await Book.findOneAndDelete({_id : req.params.id})
    if(!requiredBook){
        res.send("Requested Book Wasn't Found")
        return
    }
    res.send("Book Deleted")
})