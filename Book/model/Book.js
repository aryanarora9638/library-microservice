let mongoose = require('mongoose')

let bookSchema = new mongoose.Schema({
    title : {
        type : String,
        required : true
    },
    author : {
        type: String,
        required: true
    },
    publication : {
        type : String,
        required : true
    }
})

module.exports = mongoose.model('Book', bookSchema)

